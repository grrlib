/*===========================================
	nonameno GIT GRRLIB DEmO
===========================================*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <ogcsys.h>
#include <gccore.h>

#include "GRRLIB/GRRLIB.h"
#include "GRRLIB/fonts/font5.h"
extern u16 *GRRLIB_buffer;

#include "../gfx/nounours.h"

void (*reload)() = (void (*) ()) 0x80001800;

int main(){
	GRRLIB_buffer=(u16 *)malloc(640*480*2);	

	VIDEO_Init();
	PAD_Init();

	GRRLIB_InitVideo();
	GRRLIB_Start();
	GRRLIB_FillScreen(0x0);
	GRRLIB_Render();
	VIDEO_WaitVSync();

	while(1){
		PAD_ScanPads ();

		GRRLIB_FillScreen(0x0);
		GRRLIB_Render();
		VIDEO_WaitVSync();


		if (PAD_ButtonsDown (0) & PAD_BUTTON_START){
			reload ();
		}		
	}
return(0);

}
